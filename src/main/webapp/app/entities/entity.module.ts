import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CdmPartyPreferenceListModule } from './party-preference-list/party-preference-list.module';
import { CdmPartyPreferenceModule } from './party-preference/party-preference.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        CdmPartyPreferenceListModule,
        CdmPartyPreferenceModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CdmEntityModule {}
