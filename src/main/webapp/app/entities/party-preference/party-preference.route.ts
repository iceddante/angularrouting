import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { PartyPreferenceComponent } from './party-preference.component';
import { PartyPreferenceDetailComponent } from './party-preference-detail.component';
import { PartyPreferencePopupComponent } from './party-preference-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class PartyPreferenceResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: PaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const partyPreferenceRoute: Routes = [
    {
        path: 'party-preference',
        component: PartyPreferenceComponent,
        resolve: {
            'pagingParams': PartyPreferenceResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PartyPreferences'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const partyPreferencePopupRoute: Routes = [
    {
        path: 'party-preference-new',
        component: PartyPreferencePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PartyPreferences'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'party-preference-new/:id',
        component: PartyPreferencePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PartyPreferences'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
