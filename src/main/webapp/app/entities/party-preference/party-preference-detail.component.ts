import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager  } from 'ng-jhipster';

import { PartyPreference } from './party-preference.model';
import { PartyPreferenceService } from './party-preference.service';

@Component({
    selector: 'jhi-party-preference-detail',
    templateUrl: './party-preference-detail.component.html'
})
export class PartyPreferenceDetailComponent implements OnInit, OnDestroy {

    partyPreference: PartyPreference;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private partyPreferenceService: PartyPreferenceService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPartyPreferences();
    }

    load(id) {
        this.partyPreferenceService.find(id).subscribe((partyPreference) => {
            this.partyPreference = partyPreference;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPartyPreferences() {
        this.eventSubscriber = this.eventManager.subscribe(
            'partyPreferenceListModification',
            (response) => this.load(this.partyPreference.preferenceCode)
        );
    }
}
