import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PartyPreference } from './party-preference.model';
import { PartyPreferenceListService } from '../party-preference-list/party-preference-list.service';
import { PartyPreferenceService } from './party-preference.service';

@Injectable()
export class PartyPreferencePopupService {
    private isOpen = false;
    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private partyPreferenceService: PartyPreferenceService,
        private partyPreferenceListService: PartyPreferenceListService

    ) {}

    open(component: Component, id: string, code: string): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        this.partyPreferenceListService.find(id).subscribe((partyPreferenceList) => {
            let partyPreference: PartyPreference = null;
            for ( const pref of partyPreferenceList.partyPreferences) {
                partyPreference = pref;
            }
            return this.partyPreferenceModalRef(component, partyPreference ? partyPreference : new PartyPreference());
        });

    }

    partyPreferenceModalRef(component: Component, partyPreference: PartyPreference): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.partyPreference = partyPreference;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
