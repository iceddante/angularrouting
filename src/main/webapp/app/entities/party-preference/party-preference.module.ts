import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CdmSharedModule } from '../../shared';
import {
    PartyPreferenceService,
    PartyPreferencePopupService,
    PartyPreferenceComponent,
    PartyPreferenceDetailComponent,
    PartyPreferenceDialogComponent,
    PartyPreferencePopupComponent,
    partyPreferenceRoute,
    partyPreferencePopupRoute,
    PartyPreferenceResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...partyPreferenceRoute,
    ...partyPreferencePopupRoute,
];

@NgModule({
    imports: [
        CdmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PartyPreferenceComponent,
        PartyPreferenceDetailComponent,
        PartyPreferenceDialogComponent,
        PartyPreferencePopupComponent
    ],
    entryComponents: [
        PartyPreferenceComponent,
        PartyPreferenceDialogComponent,
        PartyPreferencePopupComponent
    ],
    providers: [
        PartyPreferenceService,
        PartyPreferencePopupService,
        PartyPreferenceResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CdmPartyPreferenceModule {}
