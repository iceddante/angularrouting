import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DateUtils } from 'ng-jhipster';

import { PartyPreference } from './party-preference.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { PartyPreferenceList } from '../party-preference-list/index';

@Injectable()
export class PartyPreferenceService {

    private resourceUrl = 'api/party-preferences';

    constructor(private http: Http, private dateUtils: DateUtils) { }

    create(partyPreference: PartyPreference): Observable<PartyPreference> {
        const copy = this.convert(partyPreference);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(partyPreference: PartyPreference): Observable<PartyPreference> {
        const copy = this.convert(partyPreference);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: string, ppl?: PartyPreferenceList): Observable<PartyPreference> {
        if(ppl) {
            // JSON list supplied. get obj from there
            for(let pref of ppl.partyPreferences) {
                if(pref.preferenceCode === id) {
                    return Observable.of(pref);
                }
            }
        }
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.effectiveFromDate = this.dateUtils
            .convertDateTimeFromServer(entity.effectiveFromDate);
        entity.effectiveToDate = this.dateUtils
            .convertDateTimeFromServer(entity.effectiveToDate);
    }

    private convert(partyPreference: PartyPreference): PartyPreference {
        const copy: PartyPreference = Object.assign({}, partyPreference);

        copy.effectiveFromDate = this.dateUtils.toDate(partyPreference.effectiveFromDate);

        copy.effectiveToDate = this.dateUtils.toDate(partyPreference.effectiveToDate);
        return copy;
    }
}
