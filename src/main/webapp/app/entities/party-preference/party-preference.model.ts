export class PartyPreference {
    constructor(
        public preferenceCode?: string,
        public effectiveFromDate?: any,
        public effectiveToDate?: any,
        public booleanPreferenceValue?: boolean,
    ) {
        this.booleanPreferenceValue = false;
    }
}
