import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { PartyPreference } from './party-preference.model';
import { PartyPreferencePopupService } from './party-preference-popup.service';
import { PartyPreferenceService } from './party-preference.service';

@Component({
    selector: 'jhi-party-preference-dialog',
    templateUrl: './party-preference-dialog.component.html'
})
export class PartyPreferenceDialogComponent implements OnInit {

    partyPreference: PartyPreference;
    authorities: any[];
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private partyPreferenceService: PartyPreferenceService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        this.subscribeToSaveResponse(
            this.partyPreferenceService.update(this.partyPreference), false);
    }

    private subscribeToSaveResponse(result: Observable<PartyPreference>, isCreated: boolean) {
        result.subscribe((res: PartyPreference) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PartyPreference, isCreated: boolean) {
        this.alertService.success('Preference updated', null, null);
        this.eventManager.broadcast({ name: 'partyPreferenceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-party-preference-popup',
    template: ''
})
export class PartyPreferencePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private partyPreferencePopupService: PartyPreferencePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
        this.modalRef = this.partyPreferencePopupService
            .open(PartyPreferenceDialogComponent, params['id'], "EOB");
        });
        /*
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.partyPreferencePopupService
                    .open(PartyPreferenceDialogComponent, params['id']);
            } else {
                this.modalRef = this.partyPreferencePopupService
                    .open(PartyPreferenceDialogComponent);
            }
        });
        */
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
