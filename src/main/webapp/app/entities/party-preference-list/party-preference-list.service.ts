import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DateUtils } from 'ng-jhipster';

import { PartyPreferenceList } from './party-preference-list.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PartyPreferenceListService {

    private resourceUrl = 'api/party-preference-lists';

    constructor(private http: Http, private dateUtils: DateUtils) { }

    create(partyPreferenceList: PartyPreferenceList): Observable<PartyPreferenceList> {
        const copy = this.convert(partyPreferenceList);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(partyPreferenceList: PartyPreferenceList): Observable<PartyPreferenceList> {
        const copy = this.convert(partyPreferenceList);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: string): Observable<PartyPreferenceList> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.modifiedDate = this.dateUtils
            .convertDateTimeFromServer(entity.modifiedDate);
    }

    private convert(partyPreferenceList: PartyPreferenceList): PartyPreferenceList {
        const copy: PartyPreferenceList = Object.assign({}, partyPreferenceList);

        copy.modifiedDate = this.dateUtils.toDate(partyPreferenceList.modifiedDate);
        return copy;
    }
}
