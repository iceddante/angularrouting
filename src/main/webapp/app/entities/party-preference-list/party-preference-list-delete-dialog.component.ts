import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { PartyPreferenceList } from './party-preference-list.model';
import { PartyPreferenceListPopupService } from './party-preference-list-popup.service';
import { PartyPreferenceListService } from './party-preference-list.service';

@Component({
    selector: 'jhi-party-preference-list-delete-dialog',
    templateUrl: './party-preference-list-delete-dialog.component.html'
})
export class PartyPreferenceListDeleteDialogComponent {

    partyPreferenceList: PartyPreferenceList;

    constructor(
        private partyPreferenceListService: PartyPreferenceListService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.partyPreferenceListService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'partyPreferenceListListModification',
                content: 'Deleted an partyPreferenceList'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success(`A Party Preference List is deleted with identifier ${id}`, null, null);
    }
}

@Component({
    selector: 'jhi-party-preference-list-delete-popup',
    template: ''
})
export class PartyPreferenceListDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private partyPreferenceListPopupService: PartyPreferenceListPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.partyPreferenceListPopupService
                .open(PartyPreferenceListDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
