import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CdmSharedModule } from '../../shared';
import {
    PartyPreferenceListService,
    PartyPreferenceListPopupService,
    PartyPreferenceListComponent,
    PartyPreferenceListDetailComponent,
    PartyPreferenceListDialogComponent,
    PartyPreferenceListPopupComponent,
    PartyPreferenceListDeletePopupComponent,
    PartyPreferenceListDeleteDialogComponent,
    partyPreferenceListRoute,
    partyPreferenceListPopupRoute,
    PartyPreferenceListResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...partyPreferenceListRoute,
    ...partyPreferenceListPopupRoute,
];

@NgModule({
    imports: [
        CdmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PartyPreferenceListComponent,
        PartyPreferenceListDetailComponent,
        PartyPreferenceListDialogComponent,
        PartyPreferenceListDeleteDialogComponent,
        PartyPreferenceListPopupComponent,
        PartyPreferenceListDeletePopupComponent,
    ],
    entryComponents: [
        PartyPreferenceListComponent,
        PartyPreferenceListDialogComponent,
        PartyPreferenceListPopupComponent,
        PartyPreferenceListDeleteDialogComponent,
        PartyPreferenceListDeletePopupComponent,
    ],
    providers: [
        PartyPreferenceListService,
        PartyPreferenceListPopupService,
        PartyPreferenceListResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CdmPartyPreferenceListModule {}
