import { PartyPreference } from '../party-preference/index';

export class PartyPreferenceList {
    constructor(
        public id?: string,
        public partyPreferences?: PartyPreference[],
        public modifiedDate?: any,
    ) {
    }
}
