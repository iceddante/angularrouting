import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager  } from 'ng-jhipster';

import { PartyPreferenceList } from './party-preference-list.model';
import { PartyPreferenceListService } from './party-preference-list.service';

@Component({
    selector: 'jhi-party-preference-list-detail',
    templateUrl: './party-preference-list-detail.component.html'
})
export class PartyPreferenceListDetailComponent implements OnInit, OnDestroy {

    partyPreferenceList: PartyPreferenceList;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private partyPreferenceListService: PartyPreferenceListService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPartyPreferenceLists();
    }

    load(id) {
        this.partyPreferenceListService.find(id).subscribe((partyPreferenceList) => {
            this.partyPreferenceList = partyPreferenceList;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPartyPreferenceLists() {
        this.eventSubscriber = this.eventManager.subscribe(
            'partyPreferenceListListModification',
            (response) => this.load(this.partyPreferenceList.id)
        );
    }
}
