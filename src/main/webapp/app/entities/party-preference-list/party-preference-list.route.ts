import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { PartyPreferenceListComponent } from './party-preference-list.component';
import { PartyPreferenceListDetailComponent } from './party-preference-list-detail.component';
import { PartyPreferenceListPopupComponent } from './party-preference-list-dialog.component';
import { PartyPreferenceListDeletePopupComponent } from './party-preference-list-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class PartyPreferenceListResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: PaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const partyPreferenceListRoute: Routes = [
    {
        path: 'party-preference-list',
        component: PartyPreferenceListComponent,
        resolve: {
            'pagingParams': PartyPreferenceListResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PartyPreferenceLists'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'party-preference-list/:id',
        component: PartyPreferenceListDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PartyPreferenceLists'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const partyPreferenceListPopupRoute: Routes = [
    {
        path: 'party-preference-list-new',
        component: PartyPreferenceListPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PartyPreferenceLists'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'party-preference-list/:id/edit',
        component: PartyPreferenceListPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PartyPreferenceLists'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'party-preference-list/:id/delete',
        component: PartyPreferenceListDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PartyPreferenceLists'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
