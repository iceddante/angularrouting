import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { PartyPreferenceList } from './party-preference-list.model';
import { PartyPreferenceListPopupService } from './party-preference-list-popup.service';
import { PartyPreferenceListService } from './party-preference-list.service';

@Component({
    selector: 'jhi-party-preference-list-dialog',
    templateUrl: './party-preference-list-dialog.component.html'
})
export class PartyPreferenceListDialogComponent implements OnInit {

    partyPreferenceList: PartyPreferenceList;
    authorities: any[];
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private partyPreferenceListService: PartyPreferenceListService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.partyPreferenceList.id !== undefined) {
            this.subscribeToSaveResponse(
                this.partyPreferenceListService.update(this.partyPreferenceList), false);
        } else {
            this.subscribeToSaveResponse(
                this.partyPreferenceListService.create(this.partyPreferenceList), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<PartyPreferenceList>, isCreated: boolean) {
        result.subscribe((res: PartyPreferenceList) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PartyPreferenceList, isCreated: boolean) {
        this.alertService.success(
            isCreated ? `A new Party Preference List is created with identifier ${result.id}`
            : `A Party Preference List is updated with identifier ${result.id}`,
            null, null);

        this.eventManager.broadcast({ name: 'partyPreferenceListListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-party-preference-list-popup',
    template: ''
})
export class PartyPreferenceListPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private partyPreferenceListPopupService: PartyPreferenceListPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.partyPreferenceListPopupService
                    .open(PartyPreferenceListDialogComponent, params['id']);
            } else {
                this.modalRef = this.partyPreferenceListPopupService
                    .open(PartyPreferenceListDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
