import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PartyPreferenceList } from './party-preference-list.model';
import { PartyPreferenceListService } from './party-preference-list.service';

@Injectable()
export class PartyPreferenceListPopupService {
    private isOpen = false;
    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private partyPreferenceListService: PartyPreferenceListService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.partyPreferenceListService.find(id).subscribe((partyPreferenceList) => {
                partyPreferenceList.modifiedDate = this.datePipe
                    .transform(partyPreferenceList.modifiedDate, 'yyyy-MM-ddThh:mm');
                this.partyPreferenceListModalRef(component, partyPreferenceList);
            });
        } else {
            return this.partyPreferenceListModalRef(component, new PartyPreferenceList());
        }
    }

    partyPreferenceListModalRef(component: Component, partyPreferenceList: PartyPreferenceList): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.partyPreferenceList = partyPreferenceList;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
